Source: ossim
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Francesco Paolo Lovergine <frankie@debian.org>
Section: science
Priority: optional
Build-Depends: cmake (>= 2.8),
               debhelper-compat (= 12),
               libcurl4-gnutls-dev | libcurl-ssl-dev,
               libfreetype6-dev,
               libgeos++-dev (>= 3.5.0),
               libgeotiff-dev,
               libjpeg-dev,
               libjsoncpp-dev,
               libpng-dev,
               libtiff-dev,
               zlib1g-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/ossim
Vcs-Git: https://salsa.debian.org/debian-gis-team/ossim.git
Homepage: https://trac.osgeo.org/ossim/

Package: libossim1
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Breaks: libossim1v5 (<< 1.8.20.3),
        libotbossimadapters-6.4-1 (<= 6.4.0+dfsg-1),
        libotbossimplugins-6.4-1 (<= 6.4.0+dfsg-1)
Replaces: libossim1v5 (<< 1.8.20.3)
Description: OSSIM library -- shared library
 Open Source Software Image Map (OSSIM) is a high performance engine for
 remote sensing, image processing, geographical information systems and
 photogrammetry. It has been actively developed since 1996.
 .
 Designed as a series of high performance software libraries, it is
 written in C++ employing the latest techniques in object-oriented
 software design.
 .
 The library provides advanced remote sensing, image processing, and
 geo-spatial functionality. A quick summary of OSSIM functionality
 includes ortho-rectification, precision terrain correction, rigorous
 sensor models, very large mosaics, and cross sensor fusions, a wide
 range of map projections and datums, and a large range of commercial
 and government data formats. The architecture of the library supports
 parallel processing with mpi (not enabled), a dynamic plugin architecture,
 and dynamically connectable objects allowing rapid prototyping of custom
 image processing chains.
 .
 This package includes the OSSIM shared library.

Package: libossim-dev
Architecture: any
Section: libdevel
Depends: libossim1 (= ${binary:Version}),
         libgeotiff-dev,
         ${misc:Depends}
Description: OSSIM library -- development files
 Open Source Software Image Map (OSSIM) is a high performance engine for
 remote sensing, image processing, geographical information systems and
 photogrammetry. It has been actively developed since 1996.
 .
 Designed as a series of high performance software libraries, it is
 written in C++ employing the latest techniques in object-oriented
 software design.
 .
 The library provides advanced remote sensing, image processing, and
 geo-spatial functionality. A quick summary of OSSIM functionality
 includes ortho-rectification, precision terrain correction, rigorous
 sensor models, very large mosaics, and cross sensor fusions, a wide
 range of map projections and datums, and a large range of commercial
 and government data formats. The architecture of the library supports
 parallel processing with mpi (not enabled), a dynamic plugin architecture,
 and dynamically connectable objects allowing rapid prototyping of custom
 image processing chains.
 .
 This package includes the development files to build programs
 that use the OSSIM library.

Package: ossim-core
Architecture: any
Depends: libossim1 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: OSSIM core utilities
 Open Source Software Image Map (OSSIM) is a high performance engine for
 remote sensing, image processing, geographical information systems and
 photogrammetry. It has been actively developed since 1996.
 .
 Designed as a series of high performance software libraries, it is
 written in C++ employing the latest techniques in object-oriented
 software design.
 .
 The library provides advanced remote sensing, image processing, and
 geo-spatial functionality. A quick summary of OSSIM functionality
 includes ortho-rectification, precision terrain correction, rigorous
 sensor models, very large mosaics, and cross sensor fusions, a wide
 range of map projections and datums, and a large range of commercial
 and government data formats. The architecture of the library supports
 parallel processing with mpi (not enabled), a dynamic plugin architecture,
 and dynamically connectable objects allowing rapid prototyping of custom
 image processing chains.
 .
 This package includes core tools that use the OSSIM library to
 perform some basic tasks.
